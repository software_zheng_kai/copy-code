<template>
  <div style="width:100%">
    <div style="height: 10vh"></div>
    <strong class="title">SHOPPING CART</strong>
    <div style="height: 3vh"></div>
    <hr
      width="80%"
      size="2"
      color="#D4D4D4"
      align="center"
      style="margin: 0 auto"
      noshade
    />
    <br/>
    <div style="width:100%">
    <div class="frame">
      <table class="tableStyle">
        <thead>
          <tr style="display: table-row; text-indent: 25%">
            <th style="padding-left:3%">消除</th>
            <th style="padding-left:4%">产品</th>
            <th style="padding-left: 9%">图像</th>
            <th style="padding-left: 11%">介绍</th>
            <div style="padding-left: 90%;">
              <th>单价</th>
              <th style="padding-left:20%">数量</th>
              <th style="padding-left:20%">总价</th>
            </div>
          </tr>
        </thead>
      </table>
      <div class="shoppingStyle">
        <div>
          <button class="deletedStyle">X</button>
          <div class="commodityName">平板电脑</div>
          <img
            src="../Image/shoppingContainerimg/commodity001.png"
            style="margin-left: -63%; margin-top: 4%"
          />
          <div class="intruduceStyle">
            华为平板HUAWEI MatePad Pro
            10.8英寸2021款鸿蒙HarmonyOS官方新品教育数码学生电脑8GB内存
          </div>
          <div style="margin-left:48%;margin-top:-5%">
              652美元
          </div>
          <div style="margin-left:67%;margin-top:-18px">1</div>
          <div style="margin-left:86%;margin-top:-18px">652美元</div>
        </div>
      </div>
      <div >
        <tr style="display: table-row;">

          <th><button>更新购物车</button></th>
          <th><button>清除购物车</button></th>
          <th><button>预计运输</button></th>
          <th><button>继续购物</button></th>
          </tr>
      </div>
    </div>
    </div>
  </div>
</template>

<script>
export default {};
</script>

<style scoped>
.title {
  font-size: 40px;
}
.frame {
  margin:0 auto;
  min-width: 80%;
  width: 80%;
  margin-left:10%;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.12), 0 0 6px rgba(0, 0, 0, 0.04);
  background-color: rgb(192, 189, 189);
  font-family: 仿宋;
  
  justify-content: space-between;
}
.tableStyle {
  vertical-align: middle;
  border-color: inherit;
  font-size: 20px;
}
th {
  color: black;
  white-space: nowrap;
  padding: 15px;
  max-width: 250px;
}
.trStyle {
  margin-left: 30px;
}
.shoppingStyle {
  width: 100%;
  height: 200px;
  background-color: white;
}
.deletedStyle {
  float: left;
  margin-left: 5%;
  margin-top: 8%;
  font-size: 25px;
}
.commodityName {
  width: 20px;
  float: left;
  margin-top: 5%;
  margin-left: 7%;
  font-size: 20px;
}
.intruduceStyle {
  width: 150px;
  height: 100px;
  margin-left: 37%;
  margin-top: -12%;
  font-size: 20px;
}

</style>










